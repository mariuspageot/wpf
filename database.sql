CREATE DATABASE  IF NOT EXISTS `projettrans` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `projettrans`;
-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: projettrans
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `commune`
--

DROP TABLE IF EXISTS `commune`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `commune` (
  `idCommune` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `idDepartement` int(11) NOT NULL,
  PRIMARY KEY (`idCommune`),
  KEY `idDep_idx` (`idDepartement`),
  CONSTRAINT `idDep` FOREIGN KEY (`idDepartement`) REFERENCES `departement` (`idDepartement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commune`
--

LOCK TABLES `commune` WRITE;
/*!40000 ALTER TABLE `commune` DISABLE KEYS */;
/*!40000 ALTER TABLE `commune` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `departement`
--

DROP TABLE IF EXISTS `departement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `departement` (
  `idDepartement` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  PRIMARY KEY (`idDepartement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departement`
--

LOCK TABLES `departement` WRITE;
/*!40000 ALTER TABLE `departement` DISABLE KEYS */;
/*!40000 ALTER TABLE `departement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ensembleplage`
--

DROP TABLE IF EXISTS `ensembleplage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ensembleplage` (
  `idEnsemblePlage` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  PRIMARY KEY (`idEnsemblePlage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ensembleplage`
--

LOCK TABLES `ensembleplage` WRITE;
/*!40000 ALTER TABLE `ensembleplage` DISABLE KEYS */;
/*!40000 ALTER TABLE `ensembleplage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ensembleplage/plage`
--

DROP TABLE IF EXISTS `ensembleplage/plage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ensembleplage/plage` (
  `idEnsemblePlage/Plage` int(11) NOT NULL,
  `idEnsemblePlage` int(11) NOT NULL,
  `idPlage` int(11) NOT NULL,
  PRIMARY KEY (`idEnsemblePlage/Plage`),
  KEY `idEnsemblePlage_idx` (`idEnsemblePlage`),
  KEY `idPlage_idx` (`idPlage`),
  CONSTRAINT `idEnsemblePlage` FOREIGN KEY (`idEnsemblePlage`) REFERENCES `ensembleplage` (`idEnsemblePlage`),
  CONSTRAINT `idPlage` FOREIGN KEY (`idPlage`) REFERENCES `plage` (`idPlage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ensembleplage/plage`
--

LOCK TABLES `ensembleplage/plage` WRITE;
/*!40000 ALTER TABLE `ensembleplage/plage` DISABLE KEYS */;
/*!40000 ALTER TABLE `ensembleplage/plage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipe`
--

DROP TABLE IF EXISTS `equipe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `equipe` (
  `idEquipe` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  `Equipecol` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idEquipe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipe`
--

LOCK TABLES `equipe` WRITE;
/*!40000 ALTER TABLE `equipe` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `espece`
--

DROP TABLE IF EXISTS `espece`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `espece` (
  `idEspece` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  PRIMARY KEY (`idEspece`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `espece`
--

LOCK TABLES `espece` WRITE;
/*!40000 ALTER TABLE `espece` DISABLE KEYS */;
/*!40000 ALTER TABLE `espece` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etude`
--

DROP TABLE IF EXISTS `etude`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `etude` (
  `idEtude` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(45) NOT NULL,
  `idEnsemblePlage` int(11) NOT NULL,
  `date` date NOT NULL,
  `coordonnée` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idEtude`),
  KEY `idEnsemblePlage_idx` (`idEnsemblePlage`),
  CONSTRAINT `idEnsemblePlage/Etude` FOREIGN KEY (`idEnsemblePlage`) REFERENCES `ensembleplage` (`idEnsemblePlage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etude`
--

LOCK TABLES `etude` WRITE;
/*!40000 ALTER TABLE `etude` DISABLE KEYS */;
/*!40000 ALTER TABLE `etude` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etude/espece`
--

DROP TABLE IF EXISTS `etude/espece`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `etude/espece` (
  `nombre` int(11) DEFAULT NULL,
  `idEspece` int(11) DEFAULT NULL,
  `idEtude` int(11) DEFAULT NULL,
  KEY `idEtude_idx` (`idEtude`),
  KEY `idEspece_idx` (`idEspece`),
  CONSTRAINT `idEspece` FOREIGN KEY (`idEspece`) REFERENCES `espece` (`idEspece`),
  CONSTRAINT `idEtude` FOREIGN KEY (`idEtude`) REFERENCES `etude` (`idEtude`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etude/espece`
--

LOCK TABLES `etude/espece` WRITE;
/*!40000 ALTER TABLE `etude/espece` DISABLE KEYS */;
/*!40000 ALTER TABLE `etude/espece` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `groupe` (
  `idGroupe` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  PRIMARY KEY (`idGroupe`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupe`
--

LOCK TABLES `groupe` WRITE;
/*!40000 ALTER TABLE `groupe` DISABLE KEYS */;
INSERT INTO `groupe` VALUES (1,'admin'),(2,'etudiant');
/*!40000 ALTER TABLE `groupe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personne`
--

DROP TABLE IF EXISTS `personne`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personne` (
  `idPersonne` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `prenom` varchar(45) NOT NULL,
  `idGroupe` int(11) NOT NULL,
  PRIMARY KEY (`idPersonne`),
  UNIQUE KEY `idPersonne_UNIQUE` (`idPersonne`),
  KEY `idGroupe_idx` (`idGroupe`),
  CONSTRAINT `idGroupe` FOREIGN KEY (`idGroupe`) REFERENCES `groupe` (`idGroupe`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personne`
--

LOCK TABLES `personne` WRITE;
/*!40000 ALTER TABLE `personne` DISABLE KEYS */;
INSERT INTO `personne` VALUES (2,'AZAZA','rfz',1),(3,'QQS','dfsd',1),(4,'QQS','dfsd',1),(5,'','',1),(6,'AZE','eeee',1),(7,'AZE','eeee',1),(8,'AZE','eeee',1),(9,'MARIUS','pageot',1),(10,'QQ','ss',1),(11,'ETUD','ett',1),(14,'X<','xx',1),(15,'KQKSFDLJK','ww',2),(16,'ETUDIAN','etudie',2);
/*!40000 ALTER TABLE `personne` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plage`
--

DROP TABLE IF EXISTS `plage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `plage` (
  `idPlage` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `idCommune` int(11) NOT NULL,
  `superficie` int(11) NOT NULL,
  PRIMARY KEY (`idPlage`),
  KEY `idCommune_idx` (`idCommune`),
  CONSTRAINT `idCommune` FOREIGN KEY (`idCommune`) REFERENCES `commune` (`idCommune`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plage`
--

LOCK TABLES `plage` WRITE;
/*!40000 ALTER TABLE `plage` DISABLE KEYS */;
/*!40000 ALTER TABLE `plage` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-10 19:05:37
